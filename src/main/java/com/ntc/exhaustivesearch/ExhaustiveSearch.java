package com.ntc.exhaustivesearch;

/**
 *
 * @author L4ZY
 */
public class ExhaustiveSearch {

    public static boolean isPartition(int[] num) {
        int sum = 0;
        for (int i = 0; i < num.length; i++) {
            sum += num[i];
        }
        if (sum % 2 != 0) {
            return false;
        }
        return findPartition(num, sum / 2, 0);
    }

    public static boolean findPartition(int[] num, int sum, int index) {
        if (sum == 0) {
            return true;
        }

        if (num.length == 0 || index >= num.length) {
            return false;
        }

        if (num[index] <= sum) {
            if (findPartition(num, sum - num[index], index + 1)) {
                return true;
            }
        }
        return findPartition(num, sum, index + 1);
    }

    public static void main(String[] args) {
        int[] num = {17, 32, 8, 21, 44, 76};
        System.out.println(isPartition(num));
    }

}
